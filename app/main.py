from flask import Flask, request, jsonify
import joblib

app = Flask(__name__)

model = joblib.load('models/trained_model.joblib')

@app.route('/predict', methods=['POST'])
def predict():
    data = request.get_json(force=True)
    review = data.get('review')

    if review is None:
        return jsonify({'error': 'Aucune critique fournie'}), 400

    prediction = model.predict([review])

    result = 'positif' if prediction[0] == 1 else 'négatif'
    return jsonify({'prediction': result})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)

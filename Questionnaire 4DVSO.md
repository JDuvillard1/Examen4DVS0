### Questionnaire MLOps - 4DVSO - Intégration Continue - M1

#### Fondamentaux DevOps et MLOps

1. **Expliquez ce que signifie DevOps et comment il facilite la collaboration entre les équipes de développement et d'opérations.**

Le DevOps est principe qui sert à relier le développement logiciel avec la mise en infrastructure de l'application de manière sécurisé et automatique.

2. **Décrivez ce qu'est le MLOps et en quoi il diffère du DevOps, surtout en ce qui concerne le cycle de vie des modèles de machine learning.**

Le MLOps est un principe similaire au DevOps qui répond plus spécifiquement au principe de Machine Learning, d'ou MLOps (Machine Learning Operations)

3. **Expliquez l'acronyme C.A.L.M.S et son importance dans le contexte DevOps**

Culture, Automatisation, Lean, Mesure, Sharing. Cet acronyme sert à fournir un cardre commun à toutes personnes pratiquant le DevOps.

4. **Listez 6 outils couramment utilisés dans la pratique du DevOps et MLOps et décrivez brièvement leur fonction.**

### Jenkins

Jenkins est un serveur d'automatisation open-source largement utilisé pour l'intégration continue et la livraison continue (CI/CD).

### Docker

Docker est une plateforme de conteneurisation qui permet de packager une application et ses dépendances dans un conteneur virtuel.

### Kubernetes

Kubernetes est un système d'orchestration de conteneurs open-source.

### GitLab CI/CD

GitLab CI/CD est un outil d'intégration et de livraison continue intégré à GitLab.

### Terraform

Terraform est un outil d'infrastructure en tant que code (IaC) qui permet de définir et de provisionner l'infrastructure informatique à l'aide de code de configuration de haut niveau. 

### MLflow

MLflow est une plateforme open-source pour la gestion du cycle de vie des modèles de machine learning.

5. **Expliquez ce qu'est l'intégration continue et son rôle.**

L'intégration continue est le fait de fournir, au sein d'un répertoire distant de manière régulière, le code d'un développeur, pour le tester de manière automatique pour maintenir un fiabilité à celui ci.

6. **Décrivez ce qu'est la livraison continue et comment elle s'intègre dans les workflows MLOps.**

La livraison continue est une pratique de développement logiciel dans laquelle les modifications de code sont automatiquement testées et préparées pour être publiées dans un environnement de production. Elle étend l'intégration continue en automatisant les étapes supplémentaires nécessaires pour déployer les modifications de code dans l'environnement de production.

7. **Expliquez le déploiement continu et sa différence avec la livraison continue.**

Le déploiement continu est une étape avancée de la livraison continue dans les pratiques DevOps.

8. **Décrivez le concept de versionnage sémantique et pourquoi il est important dans la gestion des versions de logiciels.**

Le versionnage sémantique est un système de numérotation de versions pour les logiciels qui vise à rendre les versions plus significatives et prévisibles. Le versionnage sémantique est basé sur trois numéros distincts : majeur, mineur et correctif, généralement structurés comme suit : MAJEUR.MINEUR.CORRECTIF.

9. **Listez quatre fournisseurs de services cloud et mentionnez une caractéristique unique pour chacun.**

    Amazon Web Services (AWS)

        Caractéristique Unique : Étendue du Service

    Microsoft Azure

        Caractéristique Unique : Intégration avec les Produits Microsoft

    Google Cloud Platform (GCP)

        Caractéristique Unique : Expertise en Analyse de Données et Machine Learning

    IBM Cloud
        
        Caractéristique Unique : Services Hybrides et Multicloud

10. **Définissez le service IaaS et donnez un exemple.**

Le service IaaS représente Infrastructure as a Service. Le fournisseur de service fourni la gestion du réseau, le stockage, les serveurs, et la vistualisation. l'OS, le middleware, le runtime, la donnée et les applications sont les éléments qui reste à la charge du client.

11. **Définissez le PaaS et donnez un exemple.**

Le service PaaS représent Platform as a Service, Le fournisseur de service fourni la gestion du réseau, le stockage, les serveurs, et la vistualisation, l'OS, le middleware ainsi que le runtime. La donnée et les applications sont les éléments qui reste à la charge du client.

13. **Définissez le SaaS et donnez un exemple**

Le service SaaS représente System as a Service, le fournisseur de service fourni l'intégralité du service dont le client à besoin pour travailler.

14. **Définissez le DVC, et donnez un exemple d'une command.**

Le Data Version Control (DVC) est un outil de gestion de version pour les ensembles de données et les modèles de machine learning.

Exemple de code : 

        dvc add data.csv

Pour ajouter un fichier data.csv

15. **C'est quoi un training job ?**

Un training job dans le contexte du machine learning et de l'IA fait référence au processus de formation d'un modèle de machine learning.
import unittest
import json
from app.main import app

class FlaskApiTest(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_predict_endpoint_positive_review(self):
        response = self.app.post('/predict', 
                                 data=json.dumps({'review': 'Highly recommend the amazing performances. Left me speechless.'}),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('positif', response.json['prediction'])

    def test_predict_endpoint_negative_review(self):
        response = self.app.post('/predict', 
                                 data=json.dumps({'review': 'Thoroughly enjoyed the amazing performances. A classic in the making.'}),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('négatif', response.json['prediction'])

    def test_predict_endpoint_no_review(self):
        response = self.app.post('/predict', 
                                 data=json.dumps({}),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertIn('error', response.json)

if __name__ == '__main__':
    unittest.main()